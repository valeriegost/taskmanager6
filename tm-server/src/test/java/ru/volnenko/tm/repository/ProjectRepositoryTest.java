package ru.volnenko.tm.repository;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.volnenko.tm.api.ConnectionProvider;
import ru.volnenko.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProjectRepositoryTest {


    private final PropertyService propertyService = new PropertyService();

    private final ConnectionProvider connectionProvider = new ConnectionProvider()  {
        @Override
        public Connection getConnection() {
            try {
                final String jdbcURL = propertyService.getJdbcURL();
                final String username = propertyService.getJdbcUsername();
                final String password = propertyService.getJdbcPassword();
                return DriverManager.getConnection(jdbcURL, username, password);
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    };


    @Test
    public void testCRUD() throws SQLException {
        final ProjectRepository projectRepository = new ProjectRepository(connectionProvider.getConnection());
        projectRepository.create("Project1");
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertNotNull(projectRepository.findByName("Project1"));
        Assert.assertTrue(projectRepository.findByName("Project1").getDescription() == null || projectRepository.findByName("Project1").getDescription() == "");
        projectRepository.update(projectRepository.findByName("Project1").getId(), "Project1", "This is project1");
        Assert.assertFalse(projectRepository.findByName("Project1").getDescription() == null || projectRepository.findByName("Project1").getDescription() == "");
        Assert.assertNotNull(projectRepository.removeByName("Project1"));
        Assert.assertNull(projectRepository.removeByName("Project1"));
    }


}
