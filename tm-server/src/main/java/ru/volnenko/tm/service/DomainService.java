package ru.volnenko.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.volnenko.tm.entity.Domain;
import ru.volnenko.tm.entity.Project;
import ru.volnenko.tm.entity.Task;

import java.io.*;
import java.nio.file.Files;
import java.sql.SQLException;

public class DomainService {

    private ProjectService projectService;
    private TaskService taskService;

    public DomainService(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public void load(Domain domain) throws SQLException {
        if (domain == null) return;
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());

    }

    public void save(Domain domain) throws SQLException {
        if (domain == null) return;
        projectService.clear();
        taskService.clear();
        projectService.create(domain.getProjects());
        taskService.create(domain.getTasks());
    }

    public void dataLoadJSON() throws Exception{
        final File file= new File("file.json");
        if (!file.exists()) return;
        final byte[] data = Files.readAllBytes(file.toPath());
        final String json = new String(data, "UTF-8");
        final ObjectMapper mapper = new ObjectMapper();
        final Domain domain = mapper.readValue(json, Domain.class);
        save(domain);

    }

    public void dataSaveJSON() throws Exception{
        final Domain domain = new Domain();
        load(domain);
        final ObjectMapper mapper = new ObjectMapper();
        final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        final byte[] data = json.getBytes("UTF-8");
        final File file = new File("file.json");
        Files.write(file.toPath(), data);
    }

    public void dataLoadXML() throws Exception {
        final File file = new File("file.xml");
        if (!file.exists()) return;
        final byte[] data = Files.readAllBytes(file.toPath());
        final String xml = new String(data, "UTF-8");
        final XmlMapper mapper = new XmlMapper();
        final Domain domain = mapper.readValue(xml, Domain.class);
        save(domain);
    }

    public void dataSaveXML() throws Exception {
        final Domain domain = new Domain();
        load(domain);
        final XmlMapper mapper = new XmlMapper();
        final String xml = mapper.writeValueAsString(domain);
        final byte[] data = xml.getBytes("UTF-8");
        final File file = new File("file.xml");
        Files.write(file.toPath(), data);
    }

    public void dataLoadBin() throws Exception {
        final FileInputStream fileInputStream = new FileInputStream("file.bin");
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        save(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    public void dataSaveBin() throws Exception {
        final File file = new File("file.bin");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        final Domain domain = new Domain();
        load(domain);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();

    }

}
