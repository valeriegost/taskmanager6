package ru.volnenko.tm.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.volnenko.tm.api.ConnectionProvider;
import ru.volnenko.tm.entity.Task;
import ru.volnenko.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TaskServiceTest {

    private final PropertyService propertyService = new PropertyService();

    private final ConnectionProvider connectionProvider = new ConnectionProvider()  {
        @Override
        public Connection getConnection() {
            try {
                final String jdbcURL = propertyService.getJdbcURL();
                final String username = propertyService.getJdbcUsername();
                final String password = propertyService.getJdbcPassword();
                return DriverManager.getConnection(jdbcURL, username, password);
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    };

    @Ignore // Нужно починить либо функционал либо тесты
    @Test
    public void testCRUD() throws SQLException {
        final TaskService taskService = new TaskService(connectionProvider);
        Assert.assertTrue(taskService.findAll().isEmpty());
        Task task = taskService.create("Task1");
        System.out.println("!!!!!!" + task.getName());
        Assert.assertFalse(taskService.findAll().isEmpty());
        //Assert.assertNotNull(taskService.findByName("Task1"));
        //Assert.assertNotNull(taskService.removeByName("Task1"));
        //Assert.assertNull(taskService.removeByName("Task1"));
        //Task testTask = taskService.create("Task2");
        //Assert.assertNotNull(taskService.removeById(testTask.getId()));
        //Assert.assertNull(taskService.removeById(testTask.getId()));
    }


}
