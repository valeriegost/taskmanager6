package ru.volnenko.tm.service;

import ru.volnenko.tm.api.ConnectionProvider;
import ru.volnenko.tm.entity.Project;
import ru.volnenko.tm.repository.ProjectRepository;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class ProjectService {

    private final ConnectionProvider provider;

    public ProjectService(ConnectionProvider provider) {
        this.provider = provider;
    }

    private ProjectRepository getRepository() throws SQLException {
        return new ProjectRepository(provider.getConnection());
    }

    public Project create(Project project) throws SQLException {
        if(project == null) return null;
        return getRepository().create(project);
    }

    public void create(Collection<Project> projects) throws SQLException {
        if(projects == null || projects.isEmpty()) return;
        getRepository().create(projects);
    }

    public Project create(String name) throws SQLException {
        if (name == null || name.isEmpty()) return null;
        return getRepository().create(name);
    }


    public Project update(Long id, String name, String description) throws SQLException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return getRepository().update(id, name, description);
    }

    public void clear() throws SQLException {
        getRepository().clear();
    }


    public Project findByName(String name) throws SQLException {
        return getRepository().findByName(name);
    }

    public Project findById(Long id) throws SQLException {
        return getRepository().findById(id);
    }


    public Project removeById(Long id) throws SQLException {
        return getRepository().removeById(id);
    }

    public Project removeByName(String name) throws SQLException {
        return getRepository().removeByName(name);
    }

    public List<Project> findAll() throws SQLException {
        return getRepository().findAll();
    }


}
