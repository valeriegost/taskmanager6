package ru.volnenko.tm.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.volnenko.tm.api.ConnectionProvider;
import ru.volnenko.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProjectServiceTest {


    private final PropertyService propertyService = new PropertyService();

    private final ConnectionProvider connectionProvider = new ConnectionProvider()  {
        @Override
        public Connection getConnection() {
            try {
                final String jdbcURL = propertyService.getJdbcURL();
                final String username = propertyService.getJdbcUsername();
                final String password = propertyService.getJdbcPassword();
                return DriverManager.getConnection(jdbcURL, username, password);
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    };


    @Test
    public void testCRUD() throws SQLException {
        final ProjectService projectService = new ProjectService(connectionProvider);
        projectService.create("Project1");
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertNotNull(projectService.findByName("Project1"));
        Assert.assertTrue(projectService.findByName("Project1").getDescription() == null || projectService.findByName("Project1").getDescription() == "");
        projectService.update(projectService.findByName("Project1").getId(), "Project1", "This is project1");
        Assert.assertFalse(projectService.findByName("Project1").getDescription() == null || projectService.findByName("Project1").getDescription() == "");
        Assert.assertNotNull(projectService.removeByName("Project1"));
        Assert.assertNull(projectService.removeByName("Project1"));
    }

}
