package ru.volnenko.tm.controller;

import ru.volnenko.tm.entity.Task;
import ru.volnenko.tm.service.TaskService;

import java.sql.SQLException;

public class TaskController extends AbstractController{

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    public int createTask() throws SQLException {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        taskService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() throws SQLException {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: "+ task.getId());
        System.out.println("NAME: "+ task.getName());
        System.out.println("DESCRIPTION: "+ task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() throws SQLException {
        System.out.println("ENTER, TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int listTask() throws SQLException {
        System.out.println("[LIST TASK]");
        int index = 1;
        for (final Task task: taskService.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

}
