package ru.volnenko.tm.constant;

public class TerminalConst {

    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String EXIT = "exit";

    public static final String DATA_LOAD_JSON = "data-load-json";
    public static final String DATA_SAVE_JSON = "data-save-json";
    public static final String DATA_LOAD_XML = "data-load-xml";
    public static final String DATA_SAVE_XML = "data-save-xml";
    public static final String DATA_LOAD_BIN = "data-load-bin";
    public static final String DATA_SAVE_BIN = "data-save-bin";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW = "project-view";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW = "task-view";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

}
