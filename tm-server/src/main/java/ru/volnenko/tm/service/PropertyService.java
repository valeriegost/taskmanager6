package ru.volnenko.tm.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService {

    private static final String FILE = "/application.properties";
    private static final InputStream INPUT_STREAM = PropertyService.class.getResourceAsStream(FILE);
    private static final Properties PROPERTIES = new Properties();

    {
        try {
            PROPERTIES.load(INPUT_STREAM);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public String getJdbcHost(){
        return PROPERTIES.getProperty("jdbc.host");
    }

    public String getJdbcPort(){
        return PROPERTIES.getProperty("jdbc.port");
    }

    public String getJdbcDatabase(){
        return PROPERTIES.getProperty("jdbc.database");
    }

    public String getJdbcUsername(){
        return PROPERTIES.getProperty("jdbc.username");
    }

    public String getJdbcPassword(){
        return PROPERTIES.getProperty("jdbc.password");
    }

    public String getJdbcURL(){
        return "jdbc:mysql://" + getJdbcHost() + ":" + getJdbcPort() + "/" + getJdbcDatabase();
    }
}
