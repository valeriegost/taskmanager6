package ru.volnenko.tm.endpoint;

import ru.volnenko.tm.entity.Project;
import ru.volnenko.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@WebService
public class ProjectEndpoint {

    private ProjectService projectService;
    

    public ProjectEndpoint(ProjectService projectService) {
        this.projectService = projectService;
    }


    @WebMethod
    public Project createProject(
            @WebParam(name = "name") String name
    ) throws SQLException {
        return projectService.create(name);
    }

    @WebMethod
    public Project updateProject(
            @WebParam(name = "id") Long id,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) throws SQLException {
        return projectService.update(id, name, description);
    }

    @WebMethod
    public void clearProject() throws SQLException {
        projectService.clear();
    }


    @WebMethod
    public Project findByNameProject(
            @WebParam(name = "name") String name
    ) throws SQLException {
        return projectService.findByName(name);
    }

    public Project findByIdProject(
            @WebParam(name = "id") Long id
    ) throws SQLException {
        return projectService.findById(id);
    }

    @WebMethod
    public Project removeByIdProject(
            @WebParam(name = "id") Long id
    ) throws SQLException {
        return projectService.removeById(id);
    }

    @WebMethod
    public Project removeByNameProject(
            @WebParam(name = "name") String name
    ) throws SQLException {
        return projectService.removeByName(name);
    }

    @WebMethod
    public List<Project> findAllProjects() throws SQLException {
        return projectService.findAll();
    }
}
