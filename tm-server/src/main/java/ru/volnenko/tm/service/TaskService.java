package ru.volnenko.tm.service;

import ru.volnenko.tm.api.ConnectionProvider;
import ru.volnenko.tm.entity.Task;
import ru.volnenko.tm.repository.ProjectRepository;
import ru.volnenko.tm.repository.TaskRepository;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class TaskService {

    private final ConnectionProvider provider;


    public TaskService(ConnectionProvider provider) {
        this.provider = provider;
    }

    private TaskRepository getRepository() throws SQLException {
        return new TaskRepository(provider.getConnection());
    }

    public Task create(Task task) throws SQLException {
        if(task == null) return null;
        getRepository().create(task);
        return task;
    }

    public void create(Collection<Task> tasks) throws SQLException {
        if(tasks == null || tasks.isEmpty()) return;
        getRepository().create(tasks);

    }

    public Task create(String name) throws SQLException  {
        return getRepository().create(name);
    }

    public Task findByIndex(int index) throws SQLException  {
        return getRepository().findByIndex(index);
    }

    public Task findByName(String name) throws SQLException  {
        return getRepository().findByName(name);
    }

    public Task removeById(Long id) throws SQLException  {
        return getRepository().removeById(id);
    }

    public Task removeByName(String name) throws SQLException  {
        return getRepository().removeByName(name);
    }

    public Task findById(Long id) throws SQLException  {
        return getRepository().findById(id);
    }

    public void clear() throws SQLException  {
        getRepository().clear();
    }

    public List<Task> findAll() throws SQLException  {
        return getRepository().findAll();
    }

}
