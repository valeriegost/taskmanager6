package ru.volnenko.tm.repository;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.volnenko.tm.api.ConnectionProvider;
import ru.volnenko.tm.entity.Task;
import ru.volnenko.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TaskRepositoryTest {

    private final PropertyService propertyService = new PropertyService();

    private final ConnectionProvider connectionProvider = new ConnectionProvider()  {
        @Override
        public Connection getConnection() {
            try {
                final String jdbcURL = propertyService.getJdbcURL();
                final String username = propertyService.getJdbcUsername();
                final String password = propertyService.getJdbcPassword();
                return DriverManager.getConnection(jdbcURL, username, password);
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    };

    @Test
    public void testCRUD()  throws SQLException {
        final TaskRepository taskRepository = new TaskRepository(connectionProvider.getConnection()) ;
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.create("Task1");
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(taskRepository.findByName("Task1"));
        Assert.assertNotNull(taskRepository.removeByName("Task1"));
        Assert.assertNull(taskRepository.removeByName("Task1"));
        Task testTask = taskRepository.create("Task2");
        Assert.assertNotNull(taskRepository.removeById(testTask.getId()));
        Assert.assertNull(taskRepository.removeById(testTask.getId()));
    }


}
