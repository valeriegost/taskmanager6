package ru.volnenko.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volnenko.tm.endpoint.ProjectEndpoint;
import ru.volnenko.tm.endpoint.ProjectEndpointService;
import ru.volnenko.tm.marker.SoapGroup;

public class ProjectEndpointTest {
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Test
    @Category(SoapGroup.class)
    public void testCRUD(){
        projectEndpoint.createProject("Project1", "this is project1");
        Assert.assertNotNull(projectEndpoint.findByNameProject("Project1"));
        Assert.assertNotNull(projectEndpoint.removeByNameProject("Project1"));
        Assert.assertNull(projectEndpoint.removeByNameProject("Project1"));
    }


}
