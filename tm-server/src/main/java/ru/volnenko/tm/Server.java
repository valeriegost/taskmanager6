package ru.volnenko.tm;

import ru.volnenko.tm.api.ConnectionProvider;
import ru.volnenko.tm.controller.ProjectController;
import ru.volnenko.tm.controller.SystemController;
import ru.volnenko.tm.controller.TaskController;
import ru.volnenko.tm.endpoint.ProjectEndpoint;
import ru.volnenko.tm.endpoint.TaskEndpoint;
import ru.volnenko.tm.repository.ProjectRepository;
import ru.volnenko.tm.repository.TaskRepository;
import ru.volnenko.tm.service.DomainService;
import ru.volnenko.tm.service.ProjectService;
import ru.volnenko.tm.service.PropertyService;
import ru.volnenko.tm.service.TaskService;

import javax.xml.ws.Endpoint;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Scanner;

import static ru.volnenko.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class Server {

    private final PropertyService propertyService = new PropertyService();

    private final ConnectionProvider connectionProvider = new ConnectionProvider()  {
        @Override
        public Connection getConnection() {
            try {
                final String jdbcURL = propertyService.getJdbcURL();
                final String username = propertyService.getJdbcUsername();
                final String password = propertyService.getJdbcPassword();
                return DriverManager.getConnection(jdbcURL, username, password);
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    };

    private final ProjectService projectService = new ProjectService(connectionProvider);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskService taskService = new TaskService(connectionProvider);

    private final TaskController taskController = new TaskController(taskService);

    private final DomainService domainService= new DomainService(projectService, taskService);

    private final SystemController systemController = new SystemController(domainService);





    private final ProjectEndpoint projectEndPoint = new ProjectEndpoint(projectService);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService);


    public static void main(final String[] args) throws Exception{
        final Scanner scanner = new Scanner(System.in);
        final Server server = new Server();
        server.run(args);
        Endpoint.publish("http://0.0.0.0:8080/ProjectEndpoint?wsdl", server.projectEndPoint);
        Endpoint.publish("http://0.0.0.0:8080/TaskEndpoint?wsdl", server.taskEndpoint);
        server.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            server.run(command);
        }
    }

    public void run(final String[] args) throws Exception{
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) throws Exception{
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return  systemController.displayAbout();
            case HELP: return  systemController.displayHelp();
            case EXIT: return  systemController.displayExit();

            case DATA_LOAD_JSON: return systemController.dataLoadJSON();
            case DATA_SAVE_JSON: return systemController.dataSaveJSON();
            case DATA_LOAD_XML: return systemController.dataLoadXML();
            case DATA_SAVE_XML: return systemController.dataSaveXML();
            case DATA_LOAD_BIN: return systemController.dataLoadBin();
            case DATA_SAVE_BIN: return systemController.dataSaveBin();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();

            case TASK_LIST: return taskController.listTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();

            default: return systemController.displayError();
        }
    }

}
