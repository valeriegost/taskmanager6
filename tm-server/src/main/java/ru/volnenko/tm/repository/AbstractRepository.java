package ru.volnenko.tm.repository;

import java.sql.Connection;

public class AbstractRepository {

    private Connection connection;

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection(){
        return connection;
    }
}
