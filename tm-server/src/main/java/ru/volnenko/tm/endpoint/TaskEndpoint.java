package ru.volnenko.tm.endpoint;

import ru.volnenko.tm.entity.Task;
import ru.volnenko.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@WebService
public class TaskEndpoint {

    private TaskService taskService;


    public TaskEndpoint(TaskService taskService) {
        this.taskService = taskService;
    }


    @WebMethod
    public Task createTask(
            @WebParam(name = "name") String name
    ) throws SQLException {
        return taskService.create(name);
    }

    @WebMethod
    public Task findByIndexTask(
            @WebParam(name = "index") int index
    ) throws SQLException {
        return taskService.findByIndex(index);
    }

    @WebMethod
    public Task findByNameTask(
            @WebParam(name = "name") String name
    ) throws SQLException {
        return taskService.findByName(name);
    }

    @WebMethod
    public Task removeByIdTask(
            @WebParam(name = "id") Long id
    ) throws SQLException {
        return taskService.removeById(id);
    }

    @WebMethod
    public Task removeByNameTask(
            @WebParam(name = "name") String name
    ) throws SQLException {
        return taskService.removeByName(name);
    }

    @WebMethod
    public Task findByIdTask(
            @WebParam(name = "id") Long id
    ) throws SQLException {
        return taskService.findById(id);
    }

    @WebMethod
    public void clearTask() throws SQLException {
        taskService.clear();
    }

    @WebMethod
    public List<Task> findAllTasks() throws SQLException {
        return taskService.findAll();
    }
}
