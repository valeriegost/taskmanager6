package ru.volnenko.tm.controller;

import ru.volnenko.tm.repository.ProjectRepository;
import ru.volnenko.tm.entity.Project;
import ru.volnenko.tm.service.ProjectService;

import java.sql.SQLException;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    public int createProject() throws SQLException {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectService.create(project);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName() throws SQLException {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById() throws SQLException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }


    public int clearProject() throws SQLException {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listProject() throws SQLException {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project: projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }


    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: "+ project.getId());
        System.out.println("NAME: "+ project.getName());
        System.out.println("DESCRIPTION: "+ project.getDescription());
        System.out.println("[OK]");
    }

}
