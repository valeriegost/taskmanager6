package ru.volnenko.tm.repository;

import ru.volnenko.tm.entity.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProjectRepository extends AbstractRepository{

    public ProjectRepository(Connection connection){
        super(connection);
    }

    private List<Project> projects = new ArrayList<>();

    public Project create(Project project) throws SQLException {
        final String query = "INSERT INTO app_project (id, name, description) values (?, ?, ?)";
        final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setLong(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.execute();
        return project;
    }

    public void create(Collection<Project> projects) throws SQLException {

        for(Project project : projects){
            create(project);
        }
    }

    public Project create(final String name) throws SQLException {
        return create(new Project(name));
    }


    public Project update(final Long id, final String name, final String description) throws SQLException {
        final Project project = findById(id);
        if (project == null) return null;
        final String query = "UPDATE `app_project` set `name` = ?, `description` = ? WHERE `id` = ?";
        final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setLong(3, id);
        statement.execute();
        return project;
    }

    public void clear() throws SQLException {
        final String query = "DELETE FROM `app_project`";
        final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    public Project findByName(final String name) throws SQLException {
        if (name == null || name.isEmpty()) return null;
        final String query = "SELECT * FROM `app_project` WHERE name = ?";
        final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if(!hasNext) return null;
        return fetch(resultSet);
    }

    public Project findById(final Long id) throws SQLException {
        if (id == null) return null;
        final String query = "SELECT * FROM `app_project` WHERE `id` = ?";
        final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setLong(1, id);
        final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if(!hasNext) return null;
        return fetch(resultSet);
    }


    public Project removeById(final Long id) throws SQLException {
        final Project task = findById(id);
        if (task == null) return null;
        final String query = "DELETE FROM `app_project` WHERE `id` = ?";
        final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setLong(1, task.getId());
        statement.execute();
        return task;
    }

    public Project removeByName(final String name) throws SQLException {
        final Project task = findByName(name);
        if (task == null) return null;
        final String query = "DELETE FROM `app_project` WHERE name = ?";
        final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, task.getName());
        System.out.println(statement);
        statement.execute();
        return task;
    }

    private Project fetch(final ResultSet row) throws SQLException {
        if(row == null) return null;
        final Project project = new Project();
        project.setId(row.getLong("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        return project;
    }

    public List<Project> findAll() throws SQLException {
        final Statement statement = getConnection().createStatement();
        final String query = "SELECT * FROM `app_project`";
        final ResultSet resultSet = statement.executeQuery(query);
        final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }



}
